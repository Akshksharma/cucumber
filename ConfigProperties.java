package base;

import java.io.IOException;

public class ConfigProperties {

private String Browser;
private String Url;
private String UserName;
private String Password;


public String getUrl() {
	return Url;
}

public String getUserName() {
	return UserName;
}

public String getPassword() {
	return Password;
}

public String getBrowser() {
	return Browser;
}

public ConfigProperties() throws IOException
{
	
	Browser = System.getProperty("browser");
	Url = System.getProperty("url");
	UserName = System.getProperty("username");
	Password = System.getProperty("password");
}



}
