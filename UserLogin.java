package stepDefination;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import base.Base;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.Pg_Login;

public class UserLogin extends Base{

	public static Pg_Login login = null;
	public static WebDriver driver = null;
	@Given("^Open Chrome and start MAX application$")
	public void open_Chrome_and_start_MAX_application() throws Throwable {
		setup();				
	}

	@When("^I enter the application \"([^\"]*)\"$")
	public void i_enter_the_application(String arg1) throws Throwable {
		getDriver().get(getApplicationURL());  
	}

	@Then("^application should be opened in the browser$")
	public void application_should_be_opened_in_the_browser() throws Throwable {
		login = new Pg_Login();
		Assert.assertTrue(login.txt_userName.isDisplayed());
	}

	@Given("^Application is open$")
	public void application_is_open() throws Throwable {
	    System.out.println(getDriver().getTitle()+" Loaded");
	}

	@When("^I enter \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_and(String username, String password) throws Throwable {
		login.txt_userName.sendKeys(username);
	    login.txt_passWord.sendKeys(password);
	}

	@When("^click on signIn button$")
	public void click_on_signIn_button() throws Throwable {
	    login.btn_Login.click();
	}

	@Then("^user should be able to login into system$")
	public void user_should_be_able_to_login_into_system() throws Throwable {

	}

	@Then("^Workqueue page should be displayed$")
	public void workqueue_page_should_be_displayed() throws Throwable {
	   
	}
	@Then("^Close Browser$")
	public void close_Browser() throws Throwable {
	    destroy();
	}
}
