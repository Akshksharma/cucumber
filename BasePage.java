package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import base.Base;
import base.TimeConstants;

public abstract class BasePage extends Base{

	public BasePage() {		
		PageFactory.initElements(getDriver(), this);	
		getDriver().manage().timeouts().implicitlyWait(TimeConstants.elementMinorLoadTime, TimeUnit.SECONDS);
		this.verifyPage();
	}
	
	public static void ImplicitlywaitForElement()
	{
		
		getDriver().manage().timeouts().implicitlyWait(TimeConstants.elementMinorLoadTime, TimeUnit.SECONDS);
		
		
	}
	
	public static void ExplicitlywaitForElement(WebElement element)
	{
		
		WebDriverWait wait =  new WebDriverWait(getDriver(), TimeConstants.elementLoadTime);
		wait.until(ExpectedConditions.visibilityOf(element));
		
		
	}
	
	
	public abstract void verifyPage();
}
