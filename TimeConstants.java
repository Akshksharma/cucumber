package base;

public class TimeConstants
{
    public static final long pageLoadTime = 90;
    public static final long implicitWaitTime = 30;
    public static final long elementLoadTime = 50;
    public static final long elementMinorLoadTime = 30;
    public static final int pageMajorLoadTime = 120;
    public static final int waitTime = 5;

}
