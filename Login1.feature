#Author: akshay.sharma@ocwen.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@OASIS
Feature: Login to OASIS Application

  @LoginPage
  Scenario: User should be able to open the OASIS Application
    Given Open Chrome and start OASIS application
    When I enter the application "url"
    Then application should be opened in the browser

  @ApplicationLoginProcess
  Scenario Outline: User should be able to Login into system after entering valid credentials
    Given Application is open
    When I enter "<username>" and "<password>"
    And click on signIn button
    Then user should be able to login into system
    And OASIS HOME page should be displayed
    Then Close Browser

    Examples: 
      | url                            | username | password   |
      | http://qa01.max.corp.ocwen.com | sharmaks | shefU@1234 |
