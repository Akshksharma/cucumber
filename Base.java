package base;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

public class Base{
	
	public static ConfigProperties configProperties;
	public static WebDriver driver;
	static String browser;
	static DateFormat dateFormat;
	public static Date date;


	public static void setup() throws IOException
	{
		
		configProperties = new ConfigProperties(); 		
		dateFormat = new SimpleDateFormat("M/d/yyyy");
		date = new Date();
		System.out.println(dateFormat.format(date));
		browser = configProperties.getBrowser();
		if(driver == null)
		{
			
			switch(BrowserTypes.valueOf(browser))
			{
			case CHROME:
				System.setProperty("webdriver.chrome.driver", ".\\SetupFiles\\chromedriver.exe");
				driver = new ChromeDriver();
				System.out.println("Going to launch Chrome driver!");			
				break;
				
			case IE:
				
				break;
				
			case FIREFOX:
				
				break;
			
			
			}
			
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(TimeConstants.implicitWaitTime, TimeUnit.SECONDS);
			
			
		}
	}
	
	public String getApplicationURL()
	{
		String sAppURL = "https://google.com";
		sAppURL = configProperties.getUrl();	
		return sAppURL;
	}
	
	public static WebDriver getDriver() {	
		return driver;
	}
	
	@AfterSuite
	public void destroy() {
		getDriver().quit();
		
	}

}
